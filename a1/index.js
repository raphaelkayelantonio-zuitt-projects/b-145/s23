db.users.insertOne(
{"name": "single",
 "accomodates": 2,
 "price": 1000,
 "description": "A simple room with all the basic necessities",
 "room available": 10,
 "isAvailable": false
}
);

db.users.insertMany(
	[
	{"name": "double",
 	 "accomodates": 3,
 	 "price": 2000,
 	 "description": "A room fit for a small family going on a vacation",
 	 "room available": 5,
 	 "isAvailable": false
	},
	{"name": "queen",
 	 "accomodates": 4,
 	 "price": 4000,
 	 "description": "A room with a queen sized bed perfect for a simple getaway",
 	 "room available": 15,
 	 "isAvailable": false
	}
	]
	)

db.users.find( {"name": "double"});

db.users.updateOne(
	{"name": "queen"},
	{$set: {
	 "name": "queen",
 	 "accomodates": 4,
 	 "price": 4000,
 	 "description": "A room with a queen sized bed perfect for a simple getaway",
 	 "room available": 0,
 	 "isAvailable": false
	}

}
);


db.users.deleteMany(
{
	"room available": 0
}
);