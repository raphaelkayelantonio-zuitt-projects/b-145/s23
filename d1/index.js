
db.users.insertOne(
{
	firstName: "Jane",
	lastname: "Doe",
	age: 21,
	contact: {
		phone: "89089898",
		email: "lala@gmail.com"
	},
	courses: ["css", "javascript", "python"],
	department: "none"
}
);

db.collections.insertMany([{doc1}, {doc2}, ...]);




db.collections.insertMany(
	[
	{firstName: "Jane",
	lastname: "Doe",
	age: 21,
	contact: {
		phone: "89089898",
		email: "lala@gmail.com"
	},
	courses: ["css", "javascript", "python"],
	department: "none"}, 
	{
		{firstName: "Jane",
	lastname: "Doe",
	age: 21,
	contact: {
		phone: "89089898",
		email: "lala@gmail.com"
	},
	courses: ["css", "javascript", "python"],
	department: "none"}, 
	}
	]
	);


db.collections.find({query}, {field projection})
db.users.find();

db.collections.updateOne({filter}, {update})
db.collections.updateMany()

db.users.insertOne(
{
	firstName: "test",
	lastname: "test",
	age: 0,
	contact: {
		phone: "0",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
}
);



db.user.updateOne(
	{filter},
	{update}
	);



db.user.updateOne(
	{firstname: "test"},
	{$set: {
	firstName: "bill",
	lastname: "gates",
	age: 65,
	contact: {
		phone: "12345678",
		email: "bill@gmail.com"
	},
	courses: ["php", "laravel", "html"],
	department: "operations",
	status: "active"
	}
	}
	);



db.user.updateMany({
	department: "none"
},
{	department: "HR"

}
);